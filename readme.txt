
# Setup OoT Buildroot

make -C "./buildroot/" O="$(pwd)/accel_demo/output" raspberrypi3_defconfig

make -C "./buildroot/" O="$(pwd)/accel_demo/output_std" BR2_EXTERNAL="$(pwd)/accel_demo" raspberrypi3_std_defconfig


sudo dd if=./sdcard.img of=/dev/sdc status=progress